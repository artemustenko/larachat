<?php

namespace App\Http\Controllers;

use App\Events\NewMessage;
use Chat\interfaces\ChatServiceInterface;
use Chat\MessageEvent;
use function GuzzleHttp\Promise\all;
use Illuminate\Http\Request;

class ChatController extends Controller
{
    // весь код интерфейсы, сущности
    // никакой статики хелперов глобала
    // тест на репозиторий ??
    public $chatService;

    /**
     * ChatController constructor.
     * @param ChatServiceInterface $chatServiceInstance
     */
    public function __construct(ChatServiceInterface $chatServiceInstance)
    {
        $this->chatService = $chatServiceInstance;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $rooms = $this->chatService->getChatRooms();
        return view('chat.index', compact('rooms'));
    }

    /**
     * @param Request $request
     * @return Request
     */
    public function createRoom(Request $request)
    {
        return $this->chatService->addRoom($request);
    }

    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeMessage($id, Request $request)
    {
        $message = $this->chatService->addMessage($id, $request);

        $this->chatService->broadcast($message);

        return response()->json($message);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getMessagesForRoom($id)
    {
        return $this->chatService->getMessagesForRoom($id);
    }
}
