@extends('layouts.index')

@section('title', 'Chat')

@section('css')
    <style>
        .container {
            padding-top: 30px;
        }
    </style>
@stop

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">Chat rooms</div>

                    <div class="card-body" id="app">
                        <chat :rooms="{{ $rooms }}"></chat>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

