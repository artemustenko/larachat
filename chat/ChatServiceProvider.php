<?php

namespace Chat;

use Illuminate\Support\ServiceProvider;

class ChatServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__.'/chat/migrations');

        $this->app->bind('Chat\interfaces\ChatServiceInterface', function ($app) {
            return new ChatService($this->app['events']);
        });

    }
}