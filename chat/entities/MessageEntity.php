<?php

namespace Chat\entities;


class MessageEntity
{
    public $messageEntity;

    public $room_id;
    public $user_name;
    public $message;

    private $fields = [
        'room_id',
        'user_name',
        'message',
    ];

    public function setAttributes($data)
    {
        foreach ($data as $key => $item) {
            if (in_array($key, $this->fields)){
                $this->$key = $item;
            }
        }

        return $this;
    }

    public function readyForSave()
    {
        if (!$this->validate()) {
            return false;
        }
        return true;
    }

    /**
     * Primitive validation just for structure
     *
     * @return bool
     */
    private function validate()
    {
        if (empty($this->room_id) || empty($this->user_name) || empty($this->message)){
            return false;
        }

        return true;
    }
}
