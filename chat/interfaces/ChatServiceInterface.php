<?php

namespace Chat\interfaces;

/**
 * Interface ChatServiceInterface
 * @package Chat\interfaces
 */
interface ChatServiceInterface
{
    public function getChatRooms();

    public function addRoom($request);

    public function addMessage($id, $request);

    public function broadcast($message);

    public function getMessagesForRoom($id);
}
