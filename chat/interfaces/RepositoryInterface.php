<?php

namespace Chat\interfaces;

interface RepositoryInterface
{
    public static function getRooms();

    public static function getMessagesForRoom($id);

    public static function saveRoom($request);

    public static function saveMessage($message);
}