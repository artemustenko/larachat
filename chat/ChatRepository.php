<?php

namespace Chat;

use Chat\interfaces\RepositoryInterface;
use Chat\models\Message;
use Chat\models\Room;

class ChatRepository implements RepositoryInterface
{
    /**
     * @return Room[]|\Illuminate\Database\Eloquent\Collection|mixed
     */
    public static function getRooms()
    {
        return Room::all();
    }

    /**
     * @param $id
     * @return mixed
     */
    public static function getMessagesForRoom($id)
    {
        return Message::where('room_id', $id)->get();
    }

    /**
     * @param $request
     * @return bool
     */
    public static function saveRoom($request)
    {
        if (!self::validateRoom($request)) {
            return false;
        }

        return Room::create($request->all());
    }

    /**
     *
     * @param $message
     * @return bool
     */
    public static function saveMessage($message)
    {
        return Message::create([
            'room_id' => $message->room_id,
            'user_name' => $message->user_name,
            'message' => $message->message,
        ]);
    }

    /**
     * @param $request
     * @return mixed
     */
    public static function validateRoom($request)
    {
        return $request->validate([
            'name' => 'required|string|max:255',
        ]);
    }
}