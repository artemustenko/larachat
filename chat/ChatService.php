<?php

namespace Chat;

use Chat\entities\MessageEntity;
use Chat\interfaces\ChatServiceInterface;

use Chat\models\Room;
use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;


class ChatService implements ChatServiceInterface
{
    public $broadcast;

    public function __construct(DispatcherContract $factory)
    {
        $this->broadcast = $factory;
    }

    /**
     * @return models\Room[]|\Illuminate\Database\Eloquent\Collection|mixed
     */
    public function getChatRooms()
    {
        return ChatRepository::getRooms();
    }

    /**
     * @param $request
     * @return bool
     */
    public function addRoom($request)
    {
        return ChatRepository::saveRoom($request);
    }

    /**
     * @param $id
     * @param $request
     * @return bool
     */
    public function addMessage($id, $request)
    {
        $messageEntity = new MessageEntity();

        $messageEntity->setAttributes([
            'room_id' => $id,
            'user_name' => $request->user_name,
            'message' => $request->message,
        ]);

        if (!$messageEntity->readyForSave()) {
            return false;
        }

        return ChatRepository::saveMessage($messageEntity);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getMessagesForRoom($id)
    {
        return ChatRepository::getMessagesForRoom($id);
    }

    /**
     * @param $message
     */
    public function broadcast($message)
    {
        $this->broadcast->dispatch(new MessageEvent($message));
    }


}