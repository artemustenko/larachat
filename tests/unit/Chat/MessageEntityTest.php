<?php

namespace Chat;

use Chat\entities\MessageEntity;
use Codeception\Stub\Expected;

class MessageEntityTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    public function testMessageValidation()
    {
        $array = $this->getAttributes();

        $messageEntity = new MessageEntity();

        $messageEntity->setAttributes($array);
        $this->assertTrue($messageEntity->readyForSave());

        unset($messageEntity->room_id);
        $this->assertFalse($messageEntity->readyForSave());

    }

    /**
     * something wrong
     */
    public function testDbSave() // TODO resolve problem
    {
//        $array = $this->getAttributes();
//
//        $messageEntity = new MessageEntity();
//
//        $messageEntity->setAttributes($array);
//
//        ChatRepository::saveMessage($messageEntity);
//
//        $this->tester->seeInDatabase('messages', $this->getAttributes());
    }

    public function getAttributes()
    {
        return [
            'room_id' => 12,
            'user_name' => 'John',
            'message' => 'Hello world!',
        ];
    }

}